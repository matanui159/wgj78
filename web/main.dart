import 'dart:html';
import 'src/graphics.dart';
import 'src/scene/GameScene.dart';
import 'src/scene/TextScene.dart';

void main() {
   document.body.append(LinkElement()
      ..onLoad.listen((Event) {
         graphics.scene = TextScene(GameScene(), [
            'Press any key to continue',
            'In the year 2134',
            'The sun nears the end of its life',
            'As such, it is about to explode and take the earth with it',
            'All the smartest engineers put their minds together',
            'AND TURNED THE PLANET INTO A GIANT PACMAN',
            'IF THERE IS A WILL THERE IS A WAY',
            'AMIRITE LADS',
            'Anywho, lets go take out this sun',
            'Use the left mouse button to move',
            'Just follow the yellow dot to the left'
         ]);
      })
      ..rel = 'stylesheet'
      ..href = 'https://fonts.googleapis.com/css?family=Major+Mono+Display'
   );
}