abstract class Scene {
   void update(num dt);
   void draw();
}