import 'dart:html';
import 'Scene.dart';

class Graphics {
   num width;
   num height;
   CanvasRenderingContext2D ctx;
   Scene scene;
   num _prev;

   Graphics._init() {
      var canvas = querySelector('canvas') as CanvasElement;
      ctx = canvas.context2D;

      void resize() {
         canvas
            ..width = width = window.innerWidth
            ..height = height = window.innerHeight;
      }
      window.onResize.listen((Event) => resize());
      resize();

      _prev = window.performance.now();
      _requestDraw();
   }

   void _requestDraw() {
      window.requestAnimationFrame(_draw);
   }

   void _draw(num time) {
      num dt = (time - _prev) / 1000;
      _prev = time;
      if (scene != null) {
         scene.update(dt);
         scene.draw();
      }
      _requestDraw();
   }
}

var graphics = Graphics._init();