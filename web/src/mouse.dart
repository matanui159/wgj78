import 'dart:html';
import 'graphics.dart';

class Mouse {
   num x = 0;
   num y = 0;
   bool left = false;
   bool right = false;

   void _move(MouseEvent event) {
      x = event.page.x;
      y = event.page.y;
   }

   Mouse._init() {
      window.onMouseMove.listen(_move);

      window.onContextMenu.listen((MouseEvent event) {
         event.preventDefault();
         return false;
      });

      window.onMouseDown.listen((MouseEvent event) {
         _move(event);
         switch (event.button) {
            case 0:
               left = true;
               break;
            case 2:
               right = true;
               break;
         }
      });

      window.onMouseUp.listen((MouseEvent event) {
         _move(event);
         switch (event.button) {
            case 0:
               left = false;
               break;
            case 2:
               right = false;
               break;
         }
      });
   }
}

var mouse = Mouse._init();