import 'dart:math';
import 'MoveBall.dart';
import 'EatBall.dart';
import 'EarthBall.dart';
import '../graphics.dart';

class MarsBall extends MoveBall {
   EarthBall _earth;
   
   MarsBall(this._earth, bool event) {
      var random = Random();
      var scale = graphics.height / 8 / _earth.radius;
      name = 'mars';
      radius = _earth.radius * 2 * random.nextDouble();
      var angle = random.nextDouble() * 2 * pi;
      var dist = graphics.width / scale;
      x = _earth.x + dist * cos(angle);
      y = _earth.y + dist * sin(angle);

      if (!event) {
         x = _earth.x + dist;
         y = _earth.y + (random.nextDouble() - 0.5) * graphics.height / scale;
      }
   }

   @override
   void drawBall() {
      graphics.ctx.setFillColorRgb(0xFF, 0x55, 0x55);
      graphics.ctx.fillRect(x - radius, y - radius, radius * 2, radius * 2);
   }

   @override
   void handleCollision(EatBall ball, num dist) {
      if (ball.name == 'earth') {
         super.handleCollision(ball, dist);
      } else {
         super.correctCollision(ball);
      }
   }

   @override
   void update(num dt) {
      move(5 * dt, atan2(_earth.y - y, _earth.x - x));
      super.update(dt);
   }
}