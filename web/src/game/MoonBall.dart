import 'Ball.dart';
import '../graphics.dart';

class MoonBall extends Ball {
   var angle = 0.0;
   var speed = 0.0;

   MoonBall() {
      name = 'moon';
      radius = 1;
      x = 0;
      y = 0;
   }

   @override
   void drawBall() {
      graphics.ctx.setFillColorRgb(0xAA, 0xAA, 0xAA);
      graphics.ctx.fillRect(x - radius, y - radius, 2 * radius, 2 * radius);
   }

   @override
   void update(num dt) {}
}