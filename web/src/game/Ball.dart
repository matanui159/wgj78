import 'dart:math';
import '../graphics.dart';

abstract class Ball {
   String name;
   num radius;
   num x;
   num y;

   Ball() {}

   void drawArc(num radius) {
      graphics.ctx.arc(x, y, radius, 0, pi * 2);
   }

   void drawBall();
   void update(num dt);

   void draw() {
      graphics.ctx.save();
      graphics.ctx.beginPath();
      drawArc(radius);
      graphics.ctx.clip();
      drawBall();
      graphics.ctx.setFillColorRgb(0x00, 0x00, 0x00, 0.5);
      graphics.ctx.fillRect(x - radius, y - radius, radius * 2, radius * 2);
      graphics.ctx.restore();

      graphics.ctx.save();
      graphics.ctx.beginPath();
      drawArc(radius * 0.9);
      graphics.ctx.clip();
      drawBall();
      graphics.ctx.restore();
   }

   num distance(Ball other) {
      num dx = other.x - x;
      num dy = other.y - y;
      return sqrt(dx * dx + dy * dy);
   }
}