import 'dart:math';
import 'EatBall.dart';
import 'MoveBall.dart';
import 'MoonBall.dart';
import '../graphics.dart';
import '../mouse.dart';

class EarthBall extends MoveBall {
   var moon = MoonBall();

   EarthBall() {
      name = 'earth';
      radius = 1;
      x = 0;
      y = 0;
      _updateMoon();
   }

   void _updateMoon() {
      var dist = radius * 1.5;
      moon.radius = radius / 10;
      moon.x = x + dist * cos(moon.angle);
      moon.y = y + dist * sin(moon.angle);
   }

   @override
   void drawBall() {
      graphics.ctx.setFillColorRgb(0x55, 0x55, 0xFF);
      graphics.ctx.fillRect(x - radius, y - radius, radius * 2, radius * 2);
   }

   @override
   void update(num dt) {
      if (mouse.left) {
         this.move(10 * dt, atan2(
            mouse.y - graphics.height / 2,
            mouse.x - graphics.width / 2
         ));
      }

      moon.speed = 5 * pi;
      if (mouse.right) {
         var angle = moon.angle + moon.speed * dt;
         if (angle > 2 * pi) {
            angle -= 2 * pi;
         }
         moon.angle = angle;

         for (var ball in EatBall.getAll()) {
            if (ball.name == 'mars') {
               if (ball.distance(moon) < ball.radius + moon.radius) {
                  var move = ball as MoveBall;
                  move.move(moon.speed * dt * 30, moon.angle + pi / 2);
               }
            }
         }
      }

      moon.update(dt);
      super.update(dt);
      _updateMoon();
   }

   @override
   void draw() {
      moon.draw();
      super.draw();
   }
}