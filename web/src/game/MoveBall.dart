import 'dart:math';
import 'EatBall.dart';

abstract class MoveBall extends EatBall {
   num dx = 0;
   num dy = 0;
   num mass = 1;

   @override
   void update(num dt) {
      var mul = 1 / pow(2, dt);
      dx *= mul;
      dy *= mul;
      x += dx * dt;
      y += dy * dt;
      super.update(dt);
   }

   void move(num force, num angle, [bool rad = true]) {
      var root = 1.0;
      if (rad) {
         root = sqrt(radius);
      }
      dx += cos(angle) * force * root;
      dy += sin(angle) * force * root;
   }
}