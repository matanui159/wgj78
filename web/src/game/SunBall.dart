import 'EatBall.dart';
import '../graphics.dart';

class SunBall extends EatBall {
   SunBall() {
      name = 'sun';
      radius = 100;
      x = -200;
      y = 0;
   }

   @override
   void drawBall() {
      graphics.ctx.setFillColorRgb(0xFF, 0xFF, 0x55);
      graphics.ctx.fillRect(x - radius, y - radius, radius * 2, radius * 2);
   }

   @override
   void handleCollision(EatBall ball, num dist) {
      if (ball.name == 'earth') {
         super.correctCollision(ball);
      } else {
         super.handleCollision(ball, dist);
      }
   }
}