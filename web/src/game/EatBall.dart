import 'dart:math';
import 'Ball.dart';
import '../graphics.dart';

class _Arc {
   num start;
   num end;

   _Arc(this.start, this.end);
}

abstract class EatBall extends Ball {
   static var _balls = <EatBall>[];
   static var _toDestroy = <EatBall>[];
   var destroyed = false;
   var _eating = false;
   var _arc = _Arc(0, pi * 2);

   static void updateAll(num dt) {
      for (var ball in _balls) {
         ball.update(dt);
         if (ball.destroyed) {
            _toDestroy.add(ball);
         }
      }

      while (_toDestroy.length > 0) {
         _balls.remove(_toDestroy.last);
         _toDestroy.removeLast();
      }
   }

   static void drawAll() {
      for (var ball in _balls) {
         if (!ball._eating) {
            ball.draw();
         }
      }
      for (var ball in _balls) {
         if (ball._eating) {
            ball.draw();
         }
      }
   }

   static List<EatBall> getAll() {
      return _balls;
   }

   EatBall() {
      _balls.add(this);
   }

   void destroy() {
      // _balls.remove(this);
      destroyed = true;
   }

   void handleCollision(EatBall ball, num dist) {
      _eating = true;
      if (dist <= radius - ball.radius) {
         radius = sqrt(radius * radius + ball.radius * ball.radius);
         ball.destroy();
      } else {
         var prevSize = _arc.start - _arc.end + pi * 2;
         var inside = (dist - radius + ball.radius) / (2 * ball.radius);
         var thisSize = atan2(ball.radius * sin(inside * pi), radius) * 2;
         if (thisSize > prevSize) {
            var angle = atan2(ball.y - y, ball.x - x);
            _arc = _Arc(angle + thisSize / 2, angle - thisSize / 2 + pi * 2);
         }
      }
   }

   void correctCollision(EatBall ball) {
      var angle = atan2(ball.y - y, ball.x - x);
      var dist = radius + ball.radius;
      ball.x = x + dist * cos(angle);
      ball.y = y + dist * sin(angle);
   }

   @override
   void drawArc(num radius) {
      graphics.ctx.moveTo(x, y);
      graphics.ctx.lineTo(x + radius * cos(_arc.start), y + radius * sin(_arc.start));
      graphics.ctx.arc(x, y, radius, _arc.start, _arc.end);
      graphics.ctx.lineTo(x, y);
   }

   @override
   void update(num dt) {
      _eating = false;
      _arc = _Arc(0, pi * 2);
      for (var ball in _balls) {
         if (ball.radius < radius) {
            var dist = this.distance(ball);
            if (dist < radius + ball.radius) {
               handleCollision(ball, dist);
            }
         }
      }
   }
}