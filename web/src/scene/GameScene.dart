import 'dart:math';
import '../Scene.dart';
import '../scene/TextScene.dart';
import '../game/EatBall.dart';
import '../game/EarthBall.dart';
import '../game/SunBall.dart';
import '../game/MarsBall.dart';
import '../graphics.dart';

class GameScene extends Scene {
   var earth = EarthBall();
   var sun = SunBall();
   var stars = <Point>[];
   var starClose = <num>[];

   var _eventSun = false;
   var _eventMars = false;
   var _eventLose = false;
   var _eventWin = false;

   GameScene() {
      var random = Random();
      for (var i = 0; i < 500; ++i) {
         stars.add(Point(
            random.nextDouble(),
            random.nextDouble()
         ));
         starClose.add(random.nextDouble());
      }
   }

   void _drawStars(num scale) {
      for (var i = 0; i < stars.length; ++i) {
         var star = stars[i];
         var close = starClose[i];
         graphics.ctx.beginPath();
         graphics.ctx.setFillColorRgb(0xFF, 0xFF, 0xFF, close);
         graphics.ctx.arc(
            (star.x * graphics.width - earth.x * scale * close) % graphics.width,
            (star.y * graphics.height - earth.y * scale * close) % graphics.height,
            1, 0, pi * 2
         );
         graphics.ctx.fill();
      }
   }

   @override
   void update(num dt) {
      if (_eventSun) {
         var amount = 3;
         if (_eventMars) {
            amount = 10;
         }
         while (EatBall.getAll().length < amount) {
            MarsBall(earth, _eventMars);
         }
      }
      EatBall.updateAll(dt);

      if (!_eventSun && earth.distance(sun) < 103) {
         graphics.scene = TextScene(this, [
            'Oh...',
            'Thats a problem',
            'The sun is a bit bigger than we expected',
            'What should we do now?'
         ]);
         _eventSun = true;
      }

      if (!_eventMars) {
         for (var ball in EatBall.getAll()) {
            if (ball.name == 'mars' && ball.distance(earth) < 5) {
               graphics.scene = TextScene(this, [
                  'Oh no',
                  'It is Elon Musk here to stop us',
                  'With his army of cloned Mars planets!',
                  'However...',
                  'Maybe we can use their mass',
                  'To grow big enough',
                  'To eat the Sun!',
                  'Just move into the smaller ones to eat',
                  'And if a bigger one gets to close...',
                  'Just hold down right mouse button to spin your moon',
                  'It will knock them away!',
                  'Now lets get them!'
               ]);
               _eventMars = true;
               break;
            }
         }
      }

      if (earth.destroyed) {
         if (_eventLose) {
            EatBall.getAll().clear();
            var game = GameScene();
            game._eventSun = true;
            game._eventMars = true;
            game.earth.x = -97;
            graphics.scene = game;
         } else {
            graphics.scene = TextScene(this, [
               'Welp',
               'That\'s a bummer',
               'Better luck next time',
               'Press any key to restart'
            ]);
            _eventLose = true;
         }
      }

      if (!_eventWin && sun.destroyed) {
         graphics.scene = TextScene(this, [
            'Yay!',
            'We did it!',
            'You have saved the world from the exploding sun!',
            'Now uuuhh...',
            'I guess we can just float around for a bit I guess.',
            'You know...',
            'It is a bit dark here...',
            'If you want to restart, refresh the page (ctrl+r)'
         ]);
         _eventWin = true;
      }
   }

   @override
   void draw() {
      graphics.ctx.setFillColorRgb(0x00, 0x00, 0x55);
      graphics.ctx.fillRect(0, 0, graphics.width, graphics.height);

      var scale = graphics.height / 8 / earth.radius;
      _drawStars(scale);

      graphics.ctx.save();
      graphics.ctx.translate(graphics.width / 2, graphics.height / 2);
      graphics.ctx.scale(scale, scale);
      graphics.ctx.translate(-earth.x, -earth.y);
      EatBall.drawAll();
      graphics.ctx.restore();

      if (!sun.destroyed) {
         var sunX = (sun.x - earth.x) * scale + graphics.width / 2;
         var sunY = (sun.y - earth.y) * scale + graphics.height / 2;
         graphics.ctx.beginPath();
         graphics.ctx.setFillColorRgb(0xFF, 0xFF, 0x55, 0.5);
         graphics.ctx.arc(
            min(max(sunX, 30), graphics.width - 30),
            min(max(sunY, 30), graphics.height - 30),
            10, 0, pi * 2
         );
         graphics.ctx.fill();
      }
   }
}