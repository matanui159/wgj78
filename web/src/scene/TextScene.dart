import 'dart:async';
import 'dart:html';
import '../graphics.dart';
import '../Scene.dart';

class TextScene extends Scene {
   Scene _base;
   List<String> _text;
   int _index = 0;
   StreamSubscription _keyPress;

   TextScene(this._base, this._text) {
      _keyPress = window.onKeyDown.listen((KeyboardEvent) {
         ++_index;
         if (_index == _text.length) {
            --_index;
            graphics.scene = _base;
            _keyPress.cancel();
         }
      });
   }

   @override
   void update(num dt) {
      // skip
   }

   void draw() {
      _base.draw();

      var height = 250;
      graphics.ctx.setFillColorRgb(0x00, 0x00, 0x00, 0.5);
      graphics.ctx.fillRect(0, graphics.height - height, graphics.width, height);

      graphics.ctx.setFillColorRgb(0xFF, 0xFF, 0xFF);
      graphics.ctx.font = '32px "Major Mono Display"';
      graphics.ctx.textBaseline = 'middle';

      var text = _text[_index].toLowerCase();
      var metrics = graphics.ctx.measureText(text);
      graphics.ctx.fillText(text,
         (graphics.width - metrics.width) / 2,
         graphics.height - height / 2
      );
   }
}